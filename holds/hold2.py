
    # print(commit_author)
    # print(x['created_at'])
    # print(datetime.fromisoformat(commit_date))
    created_check = datetime.strptime(x['created_at'], "%Y-%m-%dT%H:%M:%S.%fZ")
    # print(created_check)
    # 2023-04-11T18:44:49.000+00:00
    # datetime.datetime(*time.strptime("2007-03-04T21:08:12", "%Y-%m-%dT%H:%M:%S")[:6])
    # ValueError: time data '2023-04-11 18:44:49+00:00' does not match format '%Y-%m-%dT%H:%M:%S.%fZ'
    # updated_check = datetime.strptime(str(datetime.fromisoformat(commit_date)), "%Y-%m-%dT%H:%M:%S+%Z")
    updated_check = datetime.strptime((datetime.strptime(str(datetime.fromisoformat(commit_date)), "%Y-%m-%d %H:%M:%S%z")).strftime("%Y-%m-%d %H:%M:%S"),"%Y-%m-%d %H:%M:%S")
    today_second_format = datetime.strptime(today.strftime("%Y-%m-%d %H:%M:%S"),"%Y-%m-%d %H:%M:%S")
    # print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    # print(x['created_at'])
    # print(created_check)
    # print('+++++++++++++++++++++++++++')
    # print(updated_check)
    # print(today)
    # print(today_second_format)
    # print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    # updated_check = datetime.fromisoformat(commit_date)
    created_boolean = (today - created_check).days < 14
    # print(type(today))
    # print(type(created_check))
    # print(type(created_boolean))
    # print(type(today_second_format))
    # print(type(updated_check))
    updated_boolean =(today_second_format - updated_check).days < 14
    # print('007')
    stale_boolean =(today_second_format - updated_check).days < 31
    
    # checking if project has been updated in a month 
    print(stale_boolean)
    if stale_boolean:
        # check if issue already has name
        url_issue_check = 'https://gitlab.com/api/v4/projects/' + str(x['id']) + '/issues?labels=reminder'
        headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
        res_check = requests.get(url_issue_check, headers=headers)
        res_json_check = res_check.json()
        # 'author_name': 'Lars Blockken', 'author_email': 'lblockken@gitlab.com', 'authored_date': '2023-06-06T08:29:12.000+00:00', 'committer_name': 'Lars Blockken', 'committer_email': 'lblockken@gitlab.com', 'committed_date': '2023-06-06T08:29:12.000+00:00'
        print('res_json_check value: ' + str(res_json_check))
        if res_json_check == []:
            # Get the gitlabid from the author name
            url_id = 'https://gitlab.com/api/v4/users?search=' + commit_author
            headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
            res_id = requests.get(url_id, headers=headers)
            res_json_id = res_id.json()
            # 'author_name': 'Lars Blockken', 'author_email': 'lblockken@gitlab.com', 'authored_date': '2023-06-06T08:29:12.000+00:00', 'committer_name': 'Lars Blockken', 'committer_email': 'lblockken@gitlab.com', 'committed_date': '2023-06-06T08:29:12.000+00:00'
            print('&&&&&&&&&&&&&&&&&&&&&&&&&&&&')
            print(res_json_id)
            print('&&&&&&&&&&&&&&&&&&&&&&&&&&&&')
            gitlab_id = res_json_id[0]['id']

            url     = 'https://gitlab.com/api/v4/projects/' + str(project_id) + '/issues'
            headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
            if gitlab_id == 11739260:
                payload_issue = '''
            { "title": "Automated Update Reminder",
            "description": "This is an automated issue to remind the last committer to continue updating this project so that it stays relevant for others to use. If you are the most recent updater but not who should be updating this issue, please reassign it. This issue will stick until a fix is made and the issue is closed", 
            "assignee_ids": [10994493],
            "labels": "reminder",
            "due_date": "''' + two_weeks_out + '''"  }
            '''
            else:
                payload_issue = '''
            { "title": "Automated Update Reminder",
            "description": "This is an automated issue to remind the last committer to continue updating this project so that it stays relevant for others to use. If you are the most recent updater but not who should be updating this issue, please reassign it. This issue will stick until a fix is made and the issue is closed", 
            "assignee_ids": [''' + str(gitlab_id) +  ''', 10994493],
            "labels": "reminder",
            "due_date": "''' + two_weeks_out + '''"  }
            '''
            print(payload_issue)
            res_issue = requests.post(url, payload_issue, headers=headers)
            print(':::::::::')
            print(res_issue.content)
            res_json_issue = res_issue.json()
            print(';;;;;;;;;')
            print(res_json_issue)

    # Check if the compliance framework variable is empty so we dont overwrite ones used in demos
    if not x['compliance_frameworks']:
        if created_boolean :
            url     = 'https://gitlab.com/api/graphql'
            headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
            
            payload = '''
    mutation {
    projectSetComplianceFramework(
        input: {projectId: "gid://gitlab/Project/''' + str(x['id']) + '''", complianceFrameworkId: "gid://gitlab/ComplianceManagement::Framework/3498"}
    ) {
        project {
        complianceFrameworks {
            nodes {
            name
            }
        }
        }
    }
    }
            '''

            res = requests.post(url, json={"query": payload}, headers=headers)
            print(x['name'] + 'adding added label from nothing')
            print('*****************************')
            print(res.content)
            print('*****************************')
            print(payload)
        #  Created takes presidence, so only add updated if created does not apply
        elif updated_boolean and (created_boolean is False) :

            url     = 'https://gitlab.com/api/graphql'
            headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
            
            payload = '''
    mutation {
    projectSetComplianceFramework(
        input: {projectId: "gid://gitlab/Project/''' + str(x['id']) + '''", complianceFrameworkId: "gid://gitlab/ComplianceManagement::Framework/3497"}
    ) {
        project {
        complianceFrameworks {
            nodes {
            name
            }
        }
        }
    }
    }
            '''
            # res = requests.post(url, data=payload, headers=headers)
            res = requests.post(url, json={"query": payload}, headers=headers)
            print(x['name'] + 'adding updated label from nothing')
            print('*****************************')
            print(res.content)
            print('*****************************')
            print(payload)
        else:
            print('Label was empty and should be empty')
    else:
        auto_label_check = x['compliance_frameworks'] != ['Recently Added Project'] and x['compliance_frameworks'] != ['Recently Updated Project']
        # If a framework was applied, we need to make sure we dont overwrite it (unless its added or updated labels)
        print('label check result: ' + str(auto_label_check))
        print(x['compliance_frameworks'])
        print(x['compliance_frameworks'] != ['Recently Added Project'])
        print(x['compliance_frameworks'] != ['Recently Updated Project'])
        if auto_label_check:
            print('Kepping existing label (wasnt updated or added)')
        # Check if correct label is still apllied
        elif updated_boolean and  x['compliance_frameworks'] == ['Recently Updated Project']:
            print('still needs updated label')
        #Check if wrong label was applied or it used to be recently added but now is recently updated
        elif updated_boolean and  x['compliance_frameworks'] != ['Recently Updated Project'] and auto_label_check == False:
            url     = 'https://gitlab.com/api/graphql'
            headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
            
            payload = '''
    mutation {
    projectSetComplianceFramework(
        input: {projectId: "gid://gitlab/Project/''' + str(x['id']) + '''", complianceFrameworkId: "gid://gitlab/ComplianceManagement::Framework/3497"}
    ) {
        project {
        complianceFrameworks {
            nodes {
            name
            }
        }
        }
    }
    }
            '''
            # res = requests.post(url, data=payload, headers=headers)
            res = requests.post(url, json={"query": payload}, headers=headers)
            print(x['name'] + 'adding updated label from existing')
            print('*****************************')
            print(res.content)
            print('*****************************')
            print(payload)
        #  Two next statements are more of a proof check that it still should be recently updated or it wasnt mislabeled by a user
        elif created_boolean and x['compliance_frameworks'] == ['Recently Added Project']:
            print('still needs created label')
        elif created_boolean and  x['compliance_frameworks'] != ['Recently Added Project'] and auto_label_check == False:
            url     = 'https://gitlab.com/api/graphql'
            headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
            
            payload = '''
    mutation {
    projectSetComplianceFramework(
        input: {projectId: "gid://gitlab/Project/''' + str(x['id']) + '''", complianceFrameworkId: "gid://gitlab/ComplianceManagement::Framework/3498"}
    ) {
        project {
        complianceFrameworks {
            nodes {
            name
            }
        }
        }
    }
    }
            '''
            # res = requests.post(url, data=payload, headers=headers)
            res = requests.post(url, json={"query": payload}, headers=headers)
            print(x['name'] + 'adding added label from existing')
            print('*****************************')
            print(res.content)
            print('*****************************')
            print(payload)
        #  This should only remove the updated or added labels when they are no longer needed
        else:
            url     = 'https://gitlab.com/api/graphql'
            headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
            
            payload = '''
    mutation {
    projectSetComplianceFramework(
        input: {projectId: "gid://gitlab/Project/''' + str(x['id']) + '''", complianceFrameworkId: null}
    ) {
        project {
        complianceFrameworks {
            nodes {
            name
            }
        }
        }
    }
    }
            '''
            # res = requests.post(url, data=payload, headers=headers)
            res = requests.post(url, json={"query": payload}, headers=headers)
            print(x['name'] + 'removing existing added or updated label')
            print('*****************************')
            print(res.content)
            print('*****************************')
            print(payload)
    # ends the if clause
