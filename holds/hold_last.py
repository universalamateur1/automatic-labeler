if stale_boolean:
        # check if issue already has name
        url_issue_check = 'https://gitlab.com/api/v4/projects/' + str(x['id']) + '/issues?labels=reminder&state=opened'
        headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
        res_check = requests.get(url_issue_check, headers=headers)
        res_json_check = res_check.json()
        # 'author_name': 'Lars Blockken', 'author_email': 'lblockken@gitlab.com', 'authored_date': '2023-06-06T08:29:12.000+00:00', 'committer_name': 'Lars Blockken', 'committer_email': 'lblockken@gitlab.com', 'committed_date': '2023-06-06T08:29:12.000+00:00'
        print('res_json_check value: ' + str(res_json_check))
        
        if res_json_check == []:
            # Get the gitlabid from the author name
            # Get the gitlabid from the author name
            url_id = 'https://gitlab.com/api/v4/users?search=' + commit_author
            headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
            res_id = requests.get(url_id, headers=headers)
            res_json_id = res_id.json()
            # 'author_name': 'Lars Blockken', 'author_email': 'lblockken@gitlab.com', 'authored_date': '2023-06-06T08:29:12.000+00:00', 'committer_name': 'Lars Blockken', 'committer_email': 'lblockken@gitlab.com', 'committed_date': '2023-06-06T08:29:12.000+00:00'
            print('&&&&&&&&&&&&&&&&&&&&&&&&&&&&')
            print(res_json_id)
            print('***********')
            gitlab_id = 10994493

            # Get provisioned usersGET /projects/:id/users
            url_users = 'https://gitlab.com/api/v4/projects/' + str(project_id) + '/users?search=' + str(commit_author)
            print(url_users)
            headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
            res_users = requests.get(url_users, headers=headers)
            res_json_users = res_users.json()
            user_in_project = False
            # 'author_name': 'Lars Blockken', 'author_email': 'lblockken@gitlab.com', 'authored_date': '2023-06-06T08:29:12.000+00:00', 'committer_name': 'Lars Blockken', 'committer_email': 'lblockken@gitlab.com', 'committed_date': '2023-06-06T08:29:12.000+00:00'
            print('===&&&&&&&&&&&&&&&&&&&&&&&&&&&&')
            print(res_json_users)
            print('^^^^^^^^^^^')
            matching_name_ids = []
            for u in res_json_id:
                if u['name'] == commit_author:
                    # gitlab_id = u['id']
                    matching_name_ids.append(u['id'])
                    print('matching author ' + str(matching_name_ids))
            print('&&&&&&&&&&&&&&&&&&&&&&&&&&&&')

            for z in res_json_users:
                for y in matching_name_ids:
                    print(z['id'])
                    print(y)
                    print(z['id'] == y)
                    if z['id'] == y:
                        user_in_project = True
                        gitlab_id = y
            print(user_in_project)
            print(gitlab_id)
            print('===&&&&&&&&&&&&&&&&&&&&&&&&&&&&')
            print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')

            url     = 'https://gitlab.com/api/v4/projects/' + str(project_id) + '/issues'
            headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
            if gitlab_id == 11739260 or user_in_project == False:
                payload_issue = '''
            { "title": "Automated Update Reminder",
            "description": "This is an automated issue to remind the last committer to continue updating this project so that it stays relevant for others to use. If you are the most recent updater but not who should be updating this issue, please reassign it. This issue will stick until a fix is made and the issue is closed", 
            "assignee_ids": [10994493],
            "labels": "reminder",
            "due_date": "''' + two_weeks_out + '''"  }
            '''
            else:
                payload_issue = '''
            { "title": "Automated Update Reminder",
            "description": "This is an automated issue to remind the last committer to continue updating this project so that it stays relevant for others to use. If you are the most recent updater but not who should be updating this issue, please reassign it. This issue will stick until a fix is made and the issue is closed", 
            "assignee_ids": [''' + str(gitlab_id) +  ''', 10994493],
            "labels": "reminder",
            "due_date": "''' + two_weeks_out + '''"  }
            '''
            print(payload_issue)
            res_issue = requests.post(url, payload_issue, headers=headers)
            print(':::::::::')
            print(res_issue.content)
            res_json_issue = res_issue.json()
            print(';;;;;;;;;')
            print(res_json_issue)

    # Check if the compliance framework variable is empty so we dont overwrite ones used in demos
    if not x['compliance_frameworks']:
        if created_boolean :
            url     = 'https://gitlab.com/api/graphql'
            headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
            
            payload = '''
    mutation {
    projectSetComplianceFramework(
        input: {projectId: "gid://gitlab/Project/''' + str(x['id']) + '''", complianceFrameworkId: "gid://gitlab/ComplianceManagement::Framework/3498"}
    ) {
        project {
        complianceFrameworks {
            nodes {
            name
            }
        }
        }
    }
    }
            '''

            res = requests.post(url, json={"query": payload}, headers=headers)
            print(x['name'] + 'adding added label from nothing')
            print('*****************************')
            print(res.content)
            print('*****************************')
            print(payload)
        #  Created takes presidence, so only add updated if created does not apply
        elif updated_boolean and (created_boolean is False) :

            url     = 'https://gitlab.com/api/graphql'
            headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
            
            payload = '''
    mutation {
    projectSetComplianceFramework(
        input: {projectId: "gid://gitlab/Project/''' + str(x['id']) + '''", complianceFrameworkId: "gid://gitlab/ComplianceManagement::Framework/3497"}
    ) {
        project {
        complianceFrameworks {
            nodes {
            name
            }
        }
        }
    }
    }
            '''
            # res = requests.post(url, data=payload, headers=headers)
            res = requests.post(url, json={"query": payload}, headers=headers)
            print(x['name'] + 'adding updated label from nothing')
            print('*****************************')
            print(res.content)
            print('*****************************')
            print(payload)
        else:
            print('Label was empty and should be empty')
    else:
        auto_label_check = x['compliance_frameworks'] != ['Recently Added Project'] and x['compliance_frameworks'] != ['Recently Updated Project']
        # If a framework was applied, we need to make sure we dont overwrite it (unless its added or updated labels)
        print('label check result: ' + str(auto_label_check))
        print(x['compliance_frameworks'])
        print(x['compliance_frameworks'] != ['Recently Added Project'])
        print(x['compliance_frameworks'] != ['Recently Updated Project'])
        if auto_label_check:
            print('Kepping existing label (wasnt updated or added)')
        # Check if correct label is still apllied
        elif updated_boolean and  x['compliance_frameworks'] == ['Recently Updated Project']:
            print('still needs updated label')
        #Check if wrong label was applied or it used to be recently added but now is recently updated
        elif updated_boolean and  x['compliance_frameworks'] != ['Recently Updated Project'] and auto_label_check == False:
            url     = 'https://gitlab.com/api/graphql'
            headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
            
            payload = '''
    mutation {
    projectSetComplianceFramework(
        input: {projectId: "gid://gitlab/Project/''' + str(x['id']) + '''", complianceFrameworkId: "gid://gitlab/ComplianceManagement::Framework/3497"}
    ) {
        project {
        complianceFrameworks {
            nodes {
            name
            }
        }
        }
    }
    }
            '''
            # res = requests.post(url, data=payload, headers=headers)
            res = requests.post(url, json={"query": payload}, headers=headers)
            print(x['name'] + 'adding updated label from existing')
            print('*****************************')
            print(res.content)
            print('*****************************')
            print(payload)
        #  Two next statements are more of a proof check that it still should be recently updated or it wasnt mislabeled by a user
        elif created_boolean and x['compliance_frameworks'] == ['Recently Added Project']:
            print('still needs created label')
        elif created_boolean and  x['compliance_frameworks'] != ['Recently Added Project'] and auto_label_check == False:
            url     = 'https://gitlab.com/api/graphql'
            headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
            
            payload = '''
    mutation {
    projectSetComplianceFramework(
        input: {projectId: "gid://gitlab/Project/''' + str(x['id']) + '''", complianceFrameworkId: "gid://gitlab/ComplianceManagement::Framework/3498"}
    ) {
        project {
        complianceFrameworks {
            nodes {
            name
            }
        }
        }
    }
    }
            '''
            # res = requests.post(url, data=payload, headers=headers)
            res = requests.post(url, json={"query": payload}, headers=headers)
            print(x['name'] + 'adding added label from existing')
            print('*****************************')
            print(res.content)
            print('*****************************')
            print(payload)
        #  This should only remove the updated or added labels when they are no longer needed
        else:
            url     = 'https://gitlab.com/api/graphql'
            headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
            
            payload = '''
    mutation {
    projectSetComplianceFramework(
        input: {projectId: "gid://gitlab/Project/''' + str(x['id']) + '''", complianceFrameworkId: null}
    ) {
        project {
        complianceFrameworks {
            nodes {
            name
            }
        }
        }
    }
    }
            '''
            # res = requests.post(url, data=payload, headers=headers)
            res = requests.post(url, json={"query": payload}, headers=headers)
            print(x['name'] + 'removing existing added or updated label')
            print('*****************************')
            print(res.content)
            print('*****************************')
            print(payload)
    # ends the if clause
